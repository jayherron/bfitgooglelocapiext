using axon
using haystack
using skyarcd
using util
using web

class GoogleLocApiLib
{
  ** This method will return the elevation of a particular set of latitude/longitude coordinates.  It uses the Google Elevation API, outlined here: 
  ** `https://developers.google.com/maps/documentation/elevation/intro`.  If requesting points in the ocean, the depth of the ocean floor is returned.
  ** 
  ** This method requires a elevationApiKey tag on the googleApiKey record and a web connection.
  @Axon
  public static Number elevationFromCoord(Coord coordinates) {
    Str elevationKey := getApiKey("elevationApiKey")
    
    Uri uri := ("https://maps.googleapis.com/maps/api/elevation/json?locations="+coordinates.lat().toStr()+","+coordinates.lng().toStr()+"&key="+elevationKey).toUri()
    JsonInStream? jsonInStream := null
    try{jsonInStream = JsonInStream.make(WebClient(uri).getStr().in())} catch{throw Err("Unable to communicate with the Google Elevation API")}
    
    [Str:Obj?] json := jsonInStream.readJson()
    Obj[] resultList := json["results"]
    [Str:Obj?] results := resultList[0]
    if(!results.keys().contains("elevation")) throw Err("No elevation found.  See what is returned from this uri: "+uri.toStr())
    Float elevationFloat := results["elevation"]
    
    Number? elevation := null
    try elevation = Number.make(elevationFloat, Unit.fromStr("meter"))
    catch throw Err("Returned elevation could not be parsed into a float.  See what is returned from this uri: "+uri.toStr())
    
    return elevation
  }
  
  ** This method will return the address of a particular set of latitude/longitude coordinates.  It uses the Google Reverse Geocoding API, outlined here: 
  ** `https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding`.
  ** 
  ** This method requires a geocodeApiKey tag on the googleApiKey record and a web connection.
  @Axon
  public static Str addrFromCoord(Coord coordinates) {
    Str geocodeApiKey := getApiKey("geocodeApiKey")
    
    Uri uri := ("https://maps.googleapis.com/maps/api/geocode/json?latlng="+coordinates.lat().toStr()+","+coordinates.lng().toStr()+"&key="+geocodeApiKey).toUri()
    JsonInStream? jsonInStream := null
    try{jsonInStream = JsonInStream.make(WebClient(uri).getStr().in())} catch{throw Err("Unable to communicate with the Google GeoCode API")}
    
    [Str:Obj?] json := jsonInStream.readJson()
    [Str:Obj][] resultList := json["results"]
    [Str:Obj?] result := resultList[0] // The first result is the most specific.
    
    Str address := result["formatted_address"]
    
    return address
  }
  
  ** This method will return the latitude/longitude coordinates of a particular address.  It is less accurate than the address-from-coordinates lookup.
  ** Therefore, coordinates are the preferred lookup tool.
  **   
  ** It uses the Google Geocoding API, outlined here: `https://developers.google.com/maps/documentation/geocoding/intro`.
  ** 
  ** This method requires a geocodeApiKey tag on the googleApiKey record and a web connection.
  @Axon
  public static Coord coordFromAddr(Str address) {
    Str geocodeApiKey := getApiKey("geocodeApiKey")
    
    Uri uri := ("https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key="+geocodeApiKey).toUri()
    JsonInStream? jsonInStream := null
    try{jsonInStream = JsonInStream.make(WebClient(uri).getStr().in())} catch{throw Err("Unable to communicate with the Google GeoCode API")}
    
    [Str:Obj?] json := jsonInStream.readJson()
    Obj[] resultList := json["results"]
    [Str:Obj?] results := resultList[0]
    if(!results.keys().contains("geometry")) throw Err("No coordinates found.  See what is returned from this uri: "+uri.toStr())
    [Str:Obj?] geometry := results["geometry"]
    [Str:Float] location := geometry["location"]
    
    Float lat := location["lat"]
    Float lng := location["lng"]
    
    Coord? coord := null
    try coord = Coord.make(lat, lng)
    catch throw Err("Returned coordinates could not be parsed into a coord.  See what is returned from this uri: "+uri.toStr())
    
    return coord
  }
  
  ** Gets the API key from the googleApiKey record based on the input apiKeyName. Handles throwing exception when apiKeyName tag doesn't exist.
  private static Str getApiKey(Str apiKeyName){
    Dict googleApiKeys := Context.cur().read("googleApiKey")
    Str? apiKey := googleApiKeys[apiKeyName]
    if(apiKey == null) throw Err("No $apiKeyName tag exists on the googleApiKey record "+googleApiKeys.id.toCode())
    return apiKey
  }
}
