# README #

### What is this repository for? ###

This repository is an extension for the [SkySpark](https://skyfoundry.com/skyspark/) software platform.
It houses methods for extracting location data from the [Google Location APIs](https://developers.google.com/maps/documentation/).

This extension is open source and free use. The code repository is located [here](https://bitbucket.org/NeedleInAJayStack/googlelocapiext/overview).
Code contributions are welcomed and appreciated.

### How do I get set up? ###

In a running SkySpark instance, download the newest version from the Package Manager in the Host App.
Next, create a new record with a googleApiKey marker tag and input Google API keys as Str tags. This extension
currently supports the following API tags:

  - elevationApiKey
  - geocodeApiKey
  

### Who do I talk to? ###

If you have any questions, feel free to talk to Jay Herron at NeedleInAJayStack@gmail.com.


## ChangeLog ##

### 1.0.0 ###
  Extenstion creation