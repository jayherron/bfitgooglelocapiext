#! /usr/bin/env fan
using build

class Build : build::BuildPod
{
  new make()
  {
    podName = "bfitGoogleLocApiExt"
    summary = "Google Location API Extension"
    version = Version.make([1,0,0])
    
    index = [
      "skyarc.ext": "bfitGoogleLocApiExt::GoogleLocApiExt", 
      "skyarc.lib": ["bfitGoogleLocApiExt::GoogleLocApiLib"]
    ]
    meta = [
      "skyspark.docExt": "true"
    ]
    
    depends  = [
      "axon 3.0+",
      "haystack 3.0+",
      "skyarcd 3.0+",
      "sys 1.0+",
      "util 1.0+",
      "web 1.0+"
    ]

    srcDirs = [`fan/`]
    resDirs = [,]

    docApi = true
    docSrc = true
  }
}